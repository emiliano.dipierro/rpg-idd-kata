package infrastructure.repository

import domain.IdSequence

class InMemoryIdSequence(): IdSequence {
	private var sequence: Long = 0

	override fun next(): Long {
		return ++sequence
	}

}