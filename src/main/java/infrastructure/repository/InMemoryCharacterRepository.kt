package infrastructure.repository

import domain.CharacterRepository
import domain.RPGCharacter

class InMemoryCharacterRepository : CharacterRepository {
	private val characters = mutableMapOf<Long, RPGCharacter>()

	override fun save(rpgCharacter: RPGCharacter) {
		characters[rpgCharacter.id] = rpgCharacter
	}

	override fun findById(id: Long): RPGCharacter? {
		return characters[id]
	}
}