import action.CreateCharacter
import action.DealDamage
import action.HealCharacter
import infrastructure.repository.InMemoryCharacterRepository
import infrastructure.repository.InMemoryIdSequence

fun main(args: Array<String>) {

	var dealDamageActionData: DealDamage.ActionData
	var healCharacterActionData: HealCharacter.ActionData

	val sequence = InMemoryIdSequence()
	val characterRepository = InMemoryCharacterRepository()
	val createCharacter = CreateCharacter(sequence, characterRepository)
	val dealDamage = DealDamage(characterRepository)
	val healCharacter = HealCharacter(characterRepository)

	val woz = createCharacter()
	val emi = createCharacter()
	val healer = createCharacter()

	println("Woz(${woz.id}) ataca a Emi(${emi.id})")
	dealDamageActionData = DealDamage.ActionData(woz.id, emi.id)
	dealDamage(dealDamageActionData)

	characterRepository.findById(emi.id).let {
		println("La vida de emi es ${it!!.getHealth()}")
	}

	println("El healer cura a emi")
	healCharacterActionData = HealCharacter.ActionData(healer.id, emi.id)
	healCharacter(healCharacterActionData)

	characterRepository.findById(emi.id).let {
		println("La vida de emi es ${it!!.getHealth()}")
	}

	println("Venganza de Emi ataca dos veces")
	dealDamageActionData = DealDamage.ActionData(emi.id, woz.id)
	dealDamage(dealDamageActionData)
	dealDamage(dealDamageActionData)

	characterRepository.findById(woz.id).let {
		println("La vida de woz es: ${it!!.getHealth()}")
		println("esta vivo?: ${it!!.isAlive()}")
	}
}


