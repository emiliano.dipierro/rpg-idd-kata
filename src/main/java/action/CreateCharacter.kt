package action

import domain.CharacterRepository
import domain.IdSequence
import domain.RPGCharacter

class CreateCharacter(private val sequence: IdSequence,
					  private val characterRepository: CharacterRepository) {
	operator fun invoke(): RPGCharacter {
		val id = getNextId()
		return createCharacter(id).also { save(it) }
	}

	private fun getNextId() = sequence.next()

	private fun createCharacter(id: Long) = RPGCharacter(id, initialLife, initialLevel)

	private fun save(character: RPGCharacter) {
		characterRepository.save(character)
	}

	private companion object {
		const val initialLife = 1000
		const val initialLevel = 1
	}
}