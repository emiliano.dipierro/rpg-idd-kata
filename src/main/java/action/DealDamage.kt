package action

import domain.CharacterRepository
import domain.RPGCharacter

class DealDamage(private val characterRepository: CharacterRepository) {
	operator fun invoke(actionData: ActionData) {
		val (attacker, defender) = findCharacters(actionData)
		attacker.damage(defender)
		update(defender)
	}

	private fun findCharacters(actionData: ActionData): Pair<RPGCharacter, RPGCharacter> {
		val attacker = characterRepository.findById(actionData.attackerId)
		val defender = characterRepository.findById(actionData.defenderId)

		return Pair(attacker!!, defender!!)
	}

	private fun update(defender: RPGCharacter) {
		characterRepository.save(defender)
	}

	data class ActionData(val attackerId: Long, val defenderId: Long)
}