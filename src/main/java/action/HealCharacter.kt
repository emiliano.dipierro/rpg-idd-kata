package action

import domain.CharacterRepository
import domain.RPGCharacter

class HealCharacter(private val characterRepository: CharacterRepository) {
	operator fun invoke(actionData: ActionData) {
		val (healer, damaged) = findCharacters(actionData)
		healer.heal(damaged)
		characterRepository.save(damaged)
	}

	private fun findCharacters(actionData: ActionData): Pair<RPGCharacter, RPGCharacter> {
		val healer = characterRepository.findById(actionData.healerId)
		val damaged = characterRepository.findById(actionData.damagedCharacterId)

		return Pair(healer!!, damaged!!)
	}

	data class ActionData(val healerId: Long, val damagedCharacterId: Long)
}