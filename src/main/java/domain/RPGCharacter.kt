package domain

import domain.exception.DeadCharacterCannotBeHealed
import kotlin.math.min

class RPGCharacter(val id: Long, private var health: Int, val level: Int) {

	fun getHealth() = health
	fun isAlive() = health > 0

	fun damage(defender: RPGCharacter) {
		defender.receiveDamage(damage)
	}

	fun heal(damaged: RPGCharacter) {
		damaged.receiveHealing(healing)
	}

	private fun receiveDamage(damage: Int) {
		this.health = this.health - damage
	}

	private fun receiveHealing(healing: Int) {
		if (!isAlive()) throw DeadCharacterCannotBeHealed()
		this.health = min(this.health + healing, maxHealth)
	}

	private companion object {
		const val damage = 500
		const val healing = 300
		const val maxHealth = 1000
	}
}