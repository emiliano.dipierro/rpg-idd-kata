package domain

interface IdSequence {
	fun next(): Long
}