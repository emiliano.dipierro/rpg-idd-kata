package domain.exception

class DeadCharacterCannotBeHealed: RuntimeException()