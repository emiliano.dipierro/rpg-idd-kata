package domain

interface CharacterRepository {
	fun save(rpgCharacter: RPGCharacter)
	fun findById(id: Long): RPGCharacter?
}