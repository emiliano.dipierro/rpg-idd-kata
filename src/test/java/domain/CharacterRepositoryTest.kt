package domain

import infrastructure.repository.InMemoryCharacterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

abstract class CharacterRepositoryTest {

	private lateinit var repository: CharacterRepository
	private lateinit var character: RPGCharacter
	private var obtainedCharacter: RPGCharacter? = null

	@Test
	internal fun `find character by id`() {
		givenARepository()
		givenACharacter()
		givenASavedCharacter()

		whenFind()

		thenCharacterIsObtained()
	}

	@Test
	internal fun save() {
		givenARepository()
		givenACharacter()

		whenSave()

		thenCharacterIsSaved()
	}

	@Test
	internal fun update() {
		givenARepository()
		givenACharacter()
		givenASavedCharacter()

		whenUpdate()

		thenCharacterIsUpdated()
	}

	private fun givenARepository() {
		repository = createRepository()
	}

	private fun givenASavedCharacter() {
		repository.save(character)
	}

	private fun givenACharacter() {
		character = RPGCharacter(id, health, level)
	}

	private fun whenFind() {
		obtainedCharacter = repository.findById(id)
	}

	private fun whenSave() {
		repository.save(character)
	}

	private fun whenUpdate() {
		val updatedCharacter = RPGCharacter(character.id, newHealth, level)
		repository.save(updatedCharacter)
	}

	private fun thenCharacterIsSaved() {
		repository.findById(id)
	}

	private fun thenCharacterIsObtained() {
		assertThat(obtainedCharacter).isEqualTo(character)
	}

	private fun thenCharacterIsUpdated() {
		repository.findById(id).let {
			assertThat(it!!.getHealth()).isEqualTo(newHealth)
		}

	}

	abstract fun createRepository(): CharacterRepository

	private companion object {
		const val id = 1L
		const val health = 1000
		const val level = 1
		const val newHealth = 500
	}
}

class InMemoryCharacterRepositoryTest() : CharacterRepositoryTest() {
	override fun createRepository(): CharacterRepository {
		return InMemoryCharacterRepository()
	}

}

