package domain

import infrastructure.repository.InMemoryIdSequence
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class IdSequenceTest {

	private lateinit var sequence: IdSequence
	private var nextValue: Long = 0

	@Test
	internal fun `obtain next sequence`() {
		givenASequence()

		whenFindNextId()

		thenFirstIdIsReturned()
	}

	@Test
	internal fun `obtain second sequence`() {
		givenASequence()
		givenAnAlreadyRequestedId()

		whenFindNextId()

		thenSecondIdIsReturned()
	}

	private fun givenASequence() {
		sequence = InMemoryIdSequence()
	}

	private fun givenAnAlreadyRequestedId() {
		sequence.next()
	}

	private fun whenFindNextId() {
		nextValue = sequence.next()
	}

	private fun thenFirstIdIsReturned() {
		assertThat(nextValue).isEqualTo(FIRST_ID)
	}

	private fun thenSecondIdIsReturned() {
		assertThat(nextValue).isEqualTo(SECOND_ID)
	}

	private companion object {
		const val FIRST_ID = 1L
		const val SECOND_ID = 2L
	}
}