package action

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import domain.CharacterRepository
import domain.RPGCharacter
import infrastructure.repository.InMemoryIdSequence
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CreateCharacterTest {

	private lateinit var action: CreateCharacter
	private lateinit var createdCharacter: RPGCharacter
	private lateinit var existingCharacter: RPGCharacter
	private val characterRepository: CharacterRepository = mock {}

	@Test
	internal fun `created character should have initial life`() {
		givenAnAction()

		whenCreateCharacter()

		thenCharacterHasInitialLife()
	}

	@Test
	internal fun `created character should have initial level`() {
		givenAnAction()

		whenCreateCharacter()

		thenCharacterHasInitialLevel()
	}

	@Test
	internal fun `created character should be alive`() {
		givenAnAction()

		whenCreateCharacter()

		thenCharacterIsAlive()
	}

	@Test
	fun `created character should have an id`() {
		givenAnAction()

		whenCreateCharacter()

		thenCharacterHasAnId()
	}

	@Test
	fun `different characters should have differents ids`() {
		givenAnAction()
		givenACreatedCharacter()

		whenCreateCharacter()

		thenCharactersHasDifferentIds()
	}

	@Test
	fun `created character should be saved`() {
		givenAnAction()

		whenCreateCharacter()

		thenCharacterIsSaved()
	}

	private fun givenAnAction() {
		val sequence = InMemoryIdSequence()
		action = CreateCharacter(sequence, characterRepository)
	}

	private fun givenACreatedCharacter() {
		existingCharacter = action()
	}

	private fun whenCreateCharacter() {
		createdCharacter = action()
	}

	private fun thenCharacterHasInitialLife() {
		assertThat(createdCharacter.getHealth()).isEqualTo(initialLife)
	}

	private fun thenCharacterHasInitialLevel() {
		assertThat(createdCharacter.level).isEqualTo(initialLevel)
	}

	private fun thenCharacterIsAlive() {
		assertThat(createdCharacter.isAlive()).isTrue()
	}

	private fun thenCharacterHasAnId() {
		assertThat(createdCharacter.id).isGreaterThan(0)
	}

	private fun thenCharactersHasDifferentIds() {
		assertThat(createdCharacter.id).isNotEqualTo(existingCharacter.id)
	}

	private fun thenCharacterIsSaved() {
		verify(characterRepository).save(any())
	}

	private companion object {
		const val initialLife = 1000
		const val initialLevel = 1
	}

}

