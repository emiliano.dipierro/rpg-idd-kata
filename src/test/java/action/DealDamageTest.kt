package action

import domain.RPGCharacter
import infrastructure.repository.InMemoryCharacterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DealDamageTest {

	private lateinit var action: DealDamage
	private val characterRepository = InMemoryCharacterRepository()
	private lateinit var attacker: RPGCharacter
	private lateinit var defender: RPGCharacter

	@Test
	internal fun `character attack another should decrease health`() {
		givenAnAction()
		givenTwoCreatedCharacters()

		whenDealDamage()

		thenCharacterDamagedHasLessHealth()
	}

	@Test
	internal fun `character that receives deadly damage should die`() {
		givenAnAction()
		givenTwoCreatedCharacters(defenderHealth = lowHealth)

		whenDealDamage()

		thenDefenderIsDead()
	}

	private fun givenAnAction() {
		action = DealDamage(characterRepository)
	}

	private fun givenTwoCreatedCharacters(defenderHealth: Int = initialHealth) {
		attacker = RPGCharacter(attackerId, initialHealth, initialLevel)
		defender = RPGCharacter(defenderId, defenderHealth, initialLevel)
		characterRepository.save(attacker)
				.also { characterRepository.save(defender) }
	}

	private fun whenDealDamage() {
		val actionData = DealDamage.ActionData(attacker.id, defender.id)
		action(actionData)
	}

	private fun thenCharacterDamagedHasLessHealth() {
		characterRepository.findById(defenderId).let {
			assertThat(it!!.getHealth()).isEqualTo(initialHealth - damage)
		}
	}

	private fun thenDefenderIsDead() {
		characterRepository.findById(defenderId).let {
			assertThat(it!!.getHealth()).isEqualTo(0)
			assertThat(it.isAlive()).isFalse()
		}
	}

	private companion object {
		val attackerId = 1L
		val defenderId = 2L
		val initialLevel = 1
		val initialHealth = 1000
		val lowHealth = 500
		val damage = 500
	}
}