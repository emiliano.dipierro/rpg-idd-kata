package action

import domain.RPGCharacter
import domain.exception.DeadCharacterCannotBeHealed
import infrastructure.repository.InMemoryCharacterRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class HealCharacterTest {

	private lateinit var action: HealCharacter
	private val characterRepository = InMemoryCharacterRepository()
	private lateinit var healer: RPGCharacter
	private lateinit var damagedCharacter: RPGCharacter

	@Test
	internal fun `heal damaged character should increase health`() {
		givenAnAction()
		givenTwoCreatedCharacters(damagedHealth = lowHealth)

		whenHeal()

		thenCharacterHealthIsIncreased()
	}

	@Test
	internal fun `dead character can not be healed`() {
		givenAnAction()
		givenTwoCreatedCharacters(damagedHealth = deadlyHealth)

		assertThrows<DeadCharacterCannotBeHealed> { whenHeal() }
	}

	@Test
	internal fun `healed character should not be above max health`() {
		givenAnAction()
		givenTwoCreatedCharacters()

		whenHeal()

		thenHealthIsAtMaximum()
	}

	private fun givenAnAction() {
		action = HealCharacter(characterRepository)
	}

	private fun givenTwoCreatedCharacters(damagedHealth: Int = initialHealth) {
		healer = RPGCharacter(healerId, initialHealth, initialLevel)
		damagedCharacter = RPGCharacter(damagedCharacterId, damagedHealth, initialLevel)
		characterRepository.save(healer)
				.also { characterRepository.save(damagedCharacter) }
	}

	private fun whenHeal() {
		val actionData = HealCharacter.ActionData(healerId, damagedCharacterId)
		action(actionData)
	}

	private fun thenCharacterHealthIsIncreased() {
		characterRepository.findById(damagedCharacterId).let {
			assertThat(it!!.getHealth()).isEqualTo(lowHealth + healingAmount)
		}
	}

	private fun thenHealthIsAtMaximum() {
		characterRepository.findById(damagedCharacterId).let {
			assertThat(it!!.getHealth()).isEqualTo(initialHealth)
		}
	}

	private companion object {
		val healerId = 1L
		val damagedCharacterId = 2L
		val initialLevel = 1
		val initialHealth = 1000
		val lowHealth = 500
		val deadlyHealth = 0
		val damage = 500
		val healingAmount = 300
	}
}